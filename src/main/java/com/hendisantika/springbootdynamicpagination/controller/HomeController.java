package com.hendisantika.springbootdynamicpagination.controller;

import com.hendisantika.springbootdynamicpagination.entity.User;
import com.hendisantika.springbootdynamicpagination.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-dynamic-pagination
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-16
 * Time: 09:03
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class HomeController {
    private static final int PAGE_SIZE = 3;
    // total number of rows there in DB
    @Autowired
    UserRepository userRepo;
    // Number of rows to contain per page
    private long totalUsersCount;

    private PageRequest gotoPage(int page) {
        PageRequest request = PageRequest.of(page, PAGE_SIZE, Sort.Direction.DESC, "id");
        return request;
    }

    @GetMapping(value = "/")
    public String index(Model model, HttpSession session, @RequestParam(value = "pageNo", required = false, defaultValue = "0") String pageNo) {
        int lastPageNo;
        int gotoPageNo = Integer.parseInt(pageNo);
        Set<User> allUsers = new LinkedHashSet<User>();
        //session.setAttribute("currentPageNo", 0);
        for (User u : userRepo.findAll(gotoPage(gotoPageNo)))
        // fetches rows from DB as per Page No
        {
            allUsers.add(u);
        }
        totalUsersCount = userRepo.count();
        //total no of users
        if (totalUsersCount % PAGE_SIZE != 0)
            lastPageNo = (int) (totalUsersCount / PAGE_SIZE) + 1;
            // get last page No (zero based)
        else lastPageNo = (int) (totalUsersCount / PAGE_SIZE);
        model.addAttribute("lastPageNo", lastPageNo);
        model.addAttribute("users", allUsers);
        return "index";
    }
}