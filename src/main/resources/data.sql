INSERT INTO user(id, first_name, last_name, country)
VALUES (1, 'Uzumaki', 'Naruto', 'Konoha'),
       (2, 'Haruno', 'Sakura', 'Konoha'),
       (3, 'Uchiha', 'Sasuke', 'Konoha'),
       (4, 'Steve', 'Jobs', 'America'),
       (5, 'Bill', 'Gates', 'Amerika'),
       (6, 'Shah', 'Rukh Khan', 'India'),
       (7, 'Amitabh', 'Bachan', 'India'),
       (8, 'Salman', 'Khan', 'India');