<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Pagination Tutorial</title></head>
<body>
<table style="border: 1px solid black;">
    <tr style="border: 1px solid black;">
        <th>ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Country</th>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr style="border: 1px solid black;">
            <td>${user.id }</td>
            <td>${user.firstName }</td>
            <td>${user.lastName }</td>
            <td>${user.country }</td>
        </tr>
    </c:forEach></table>
<br> <br>
<c:forEach var="i" begin="0" end="${lastPageNo-1 }"> <a
        href="/?pageNo=${i }">${i+1 }</a>&nbsp;&nbsp;&nbsp;&nbsp;
    <!-- Displaying Page No -->
</c:forEach></body>
</html>